import os
import subprocess
import pytesseract
from PIL import Image

os.system("ls -la *.png | awk '{print $9}' | cat > a.txt")

file = open("a.txt")

lines = []

y = 0

for line in file.readlines():
    lines.append(line)
    y = y + 1
    print str(y) + " " + line

num = raw_input("What file do you want to get text from?\n")

y = 0

while (y < len(lines)):
    lines[y] = lines[y].rstrip('\n')
    y = y + 1

print(pytesseract.image_to_string(Image.open(lines[int(num.rstrip()) - 1])))